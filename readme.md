# IFOM-Pat

IFOM-Pat is a software written in python with the django framework.

IFOM-Pat is a ontological software that helps with identifying types of metal in a non destructive way.

# Build

## Requirements

Mise à part pour le build manuel, le projet a besoin de docker et de docker-compose

### Debian/ubuntu

`sudo apt install docker docker-compose`

### Arch

`sudo pacman -S docker docker-compose`

## Docker

For Docker, one just need to start the provided docker-compose file

## Manual

For a manual build you need the following prequisites :

- Python3
- python3-pip
- python3-venv
- Java

1. Create a virtual environnement

   `python3 -m venv venv`
2. Activate the virtual environement

   `source venv/bin/activate`
3. pip install the requirements from the provided requirements file

   `pip install -r requirements.txt`

# Run

## Docker

The provided docker-compose automatically start the server

## Manual

While the virtual environement is activated, simply start the server :

  `python3 TestOntologie4/manage.py runserver`

# Use

After launching, the app is accessed from a web browser on http://localhost:8000
