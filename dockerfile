FROM debian:buster
RUN apt update && apt install -y java-common python3 python3-pip python3-venv python3-wheel default-jre

ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

RUN pip install wheel

COPY requirements.txt .
RUN pip install -r requirements.txt

CMD cd /opt/ifompat && python manage.py runserver 0.0.0.0:8000
