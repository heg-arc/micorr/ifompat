from django.shortcuts import render
from owlready2 import *

ONTOLOGIE = "static/ifom_pat/ontology/OntoMiCorrVisualObservationV2.10.owl"
onto = get_ontology(ONTOLOGIE).load()

firstQuestion = onto.The_surface_of_the_metal_you_are_studing_is

metalsList = onto.Metals.instances()
questionAndAnswersList = []
questionAndAnswerListForContext = []
suggestedMetalsList = []
notSuggestedMetalsList = []
decisionChainList = []
decisionChainListForContext = []
suggestedMetalsListForContext = []
notSuggestedMetalsListForContext = []
searchHistoryListForContext = []

listOfQuestionAndAnswerListForContext = []



def index(request):

    return render(request, 'ifom_pat/index.html')


def sequentialQuestion(request):

    global questionAndAnswerListForContext
    global suggestedMetalsListForContext
    global notSuggestedMetalsListForContext
    global decisionChainList
    global decisionChainListForContext
    global searchHistoryListForContext
    global suggestedMetalsList
    global notSuggestedMetalsList

    answerQuery, questionQuery, backQuery = getQueryGET(request)

    if not (answerQuery and questionQuery):

        """
        On passe la première question dans le contexte
        """
        question = firstQuestion
        questionAndAnswerListForContext = []
        questionAndAnswerListForContext = getQuestionAndAnswersForContextByQuestion(question)

        nextQuestionExists = True
        isInconclusive = False

        decisionChainList = []
        decisionChainListForContext = []
        suggestedMetalsListForContext = []
        notSuggestedMetalsListForContext = []
        searchHistoryListForContext = []

    else:

        isInconclusive = False
        nextQuestionExists = True
        questionAndAnswerListForContext = []

        """
        On récupère à partir de la requète la question et la réponse
        """
        question = getQuestionByQuestionQuery(questionQuery)
        answer = getAnswerByAnswerQuery(answerQuery)

        """
        backQuery passe à yes lorsque l'utilisateur clique sur le lien d'un élément de la chaine décisionnelle
        """
        if backQuery == 'yes':
            """
            On créé un historique de sa recherche précédente
            """
            searchHistoryListForContext = getSearchHistoryList(decisionChainList,suggestedMetalsList, notSuggestedMetalsList)
            """
            On reprend la chaine décisionnelle à l'élément précédent celui sur lequel l'utilisateur à cliqué
            """
            decisionChainList = getNewDecisionChainList(decisionChainList, questionQuery)
            decisionChainListForContext = getDecisionChainListOfListForContext(decisionChainList)
            """
            La question passé en requète est celle sur laquelle l'utilisateur a cliqué
            """
            questionAndAnswerListForContext = getQuestionAndAnswersForContextByQuestion(question)
            """
            On passe les listes de métaux dans le contexte
            """
            notSuggestedMetalsList = list(metalsList).copy()
            nextElementsList = question.questionHasForNextElements
            suggestedMetalsList, notSuggestedMetalsList = getMetalsList(nextElementsList, notSuggestedMetalsList)

        else:
            """
            La chaine décisionnelle est incrémenté de la question et de la réponse donnée
            """
            decisionChainList.append(getDecisionChainMap(question, answer))
            decisionChainListForContext = getDecisionChainListOfListForContext(decisionChainList)
            """
            On affecte les valeurs de la question et de la réponse à l'individu research 
            """
            research = onto.Research()
            research.researchHasForQuestion.append(question)
            research.researchHasForSelectedAnswer.append(answer)
            sync_reasoner(infer_property_values=True)

            nextQuestion, nextElementsList, nextQuestionExists = getNextElements(research)
            if nextQuestionExists:
                questionAndAnswerListForContext = getQuestionAndAnswersForContextByQuestion(nextQuestion[0])
            notSuggestedMetalsList = list(metalsList).copy()
            suggestedMetalsList, notSuggestedMetalsList = getMetalsList(nextElementsList, notSuggestedMetalsList)

        if len(suggestedMetalsList) == 0:
            isInconclusive = True
            suggestedMetalsListForContext = []
            suggestedMetalsListForContext.append(['Inconclusive', 'NoDocSheet'])
        else:
            suggestedMetalsListForContext = getMetalsListForContext(suggestedMetalsList)
        notSuggestedMetalsListForContext = getMetalsListForContext(notSuggestedMetalsList)


    context = {
        'questionAndAnswerListForContext': questionAndAnswerListForContext,
        'nextQuestionExists': nextQuestionExists,
        'isInconclusive': isInconclusive,
        'suggestedMetalsListForContext': suggestedMetalsListForContext,
        'notSuggestedMetalsListForContext': notSuggestedMetalsListForContext,
        'decisionChainListForContext': decisionChainListForContext,
        'searchHistoryListForContext': searchHistoryListForContext,
    }

    return render(request, 'ifom_pat/sequential_questions.html', context)


def listOfQuestions(request):

    global listOfQuestionAndAnswerListForContext
    global suggestedMetalsListForContext
    global notSuggestedMetalsListForContext
    global decisionChainListForContext
    global decisionChainList

    answerQuery, questionQuery, backQuery = getQueryGET(request)

    if not (answerQuery and questionQuery):

        """
        On récupère toutes les questions de l'ontologie, on les ordonnes et on les ajoutes à questionsList
        """
        questionsList = onto.Questions.instances()
        questionsList.sort(key=getDisplayIndex)
        """
        On passe questionList dans le contexte
        """
        listOfQuestionAndAnswerListForContext = getListOfQuestionAndAnswerListForContext(questionsList)
        decisionChainList = []
        decisionChainListForContext = []
        suggestedMetalsListForContext = []
        notSuggestedMetalsListForContext = []

    else:

        listOfQuestionAndAnswerListForContext = []

        """
        On récupère la ou les questions à partir de questionQuery
        On récupère la réponse qui correspond à chaque question à partir de answerQuery
        On ajoute la question et la réponse à la liste questionAndAnswer
        """
        questions = onto.search(type=onto.Questions, hasForDisplay=questionQuery)
        questionAndAnswer = []
        for question in questions:
            tempMap = {}
            answerList = getAnswersList(question)
            if len(answerList) == 1:
                answer = answerList[0]
            else:
                answer = list(filter(lambda x: x.hasForDisplay[0] == answerQuery, answerList))[0]
            tempMap['question'] = question
            tempMap['answer'] = answer
            questionAndAnswer.append(tempMap)

        """
        On affecte research avec la question et la réponse
        et on lance le raisonneur
        """
        research = onto.Research()
        for element in questionAndAnswer:
            research.researchHasForQuestion.append(element.get('question'))
            research.researchHasForSelectedAnswer.append(element.get('answer'))
        sync_reasoner(infer_property_values=True)

        nextElementsList = []
        previousQuestionAndAnswersList = []

        if len(decisionChainList) == 0:
            """
            Si la chaine de décision est vide, on y ajoute la ou les questions
            """
            decisionChainList = questionAndAnswer
            """
            Si la question est simple, on récupère tous les éléments suivants qu'on ajoute à nextElementList
            Si la question est multiple, on récupère:
             - tous les éléments suivant, 
             - toutes les questions précédentes, avec les seuls réponses possible en fonction de la réponse donnée
            On les ajoutes à nextElementsList           
            """
            if len(questionAndAnswer) == 1:
                nextElementsList = research.researchHasForNextElementS
            elif len(questionAndAnswer) > 1:
                nextElementsList = research.researchHasForNextElementS
                previousAnswerList = research.researchHasForPreviousAnswers
                myQuestionList = []

                for answer in previousAnswerList:
                    if answer.answerHasForQuestion not in myQuestionList:
                        myQuestionList.append(answer.answerHasForQuestion)
                previousQuestionAndAnswersList = []
                for question in myQuestionList:
                    nextElementsList.append(question[0])
                    tempList = []
                    for answer in previousAnswerList:
                        if answer.answerHasForQuestion == question:
                            tempList.append(answer)
                    tempList.sort(key=getDisplayIndex)
                    previousQuestionAndAnswersList.append([question[0], tempList])

        else:
            """
            Si la chaine de decision n'est pas vide,
            on récupère tous les éléments suivants la/les questions/réponses données.
            Et parmi les éléments suivants, on récupère ceux qui existe dans decisionChainList,
            et on les ajoute à myList
            """
            nextElementsList = research.researchHasForNextElementS
            questionInDecisionChainList = list(map(lambda x: x.get('question'), decisionChainList))
            myList = list(filter(lambda x: x in questionInDecisionChainList, nextElementsList))

            """
            Si myList est vide --> dernière question/reponse est plus avancé dans la chaine décisionnel 
            que tous les element de decisionChainList
            Si myList n'est pas vide --> des éléments de decisionChainList sont plus avancé 
            que la dernière question/reponse 
            """
            if len(myList) == 0:
                """
                Si question/reponse est unique, on ajoute question/réponse à decisionChainList
                et on ajoute tous les éléments suivants la derniere question/reponse à nextElementsList
                Si question/reponse est multiple, on ajoute tous les éléments suivants la derniere question/reponse à nextElementsList,
                et pour chaque élément de decisionCHainList, on récupère tous les éléments suivants
                """
                if len(questionAndAnswer) == 1:
                    decisionChainList.append(questionAndAnswer[0]) # ATTENTION VOIR POUR QUAND MEME EPURER DECISIONCHAINLIST EN FONCTION QUESTION/REPONSE
                    nextElementsList = research.researchHasForNextElementS

                elif len(questionAndAnswer) > 1:
                    nextElementsList = research.researchHasForNextElementS
                    myNextQuestionsList = []
                    answersInDecisionChainList = list(map(lambda x: x.get('answer'), decisionChainList))
                    nextQuestionInDecisionChainList = []
                    for answer in answersInDecisionChainList:
                        if answer.answerHasForNextElement[0].is_a[0] != onto.Metals:
                            nextQuestionInDecisionChainList.append(answer.answerHasForNextElement[0])
                    for question in nextQuestionInDecisionChainList:
                        myNextElementsList = question.questionHasForNextElements
                        myNextQuestionsList, myNextMetalsList = sortNextElementsList(myNextElementsList)

                    previousAnswerList = research.researchHasForPreviousAnswers
                    questionList = []
                    """
                    Pour chaque reponse precedente la dernière question/réponse, 
                    si elle fait partie des éléments suivants la decisionChainList,
                    on l'ajoute à nextElementsList, et on garde seulement ses réponses possible
                    """
                    for answer in previousAnswerList:
                        if (answer.answerHasForQuestion[0] in myNextQuestionsList) and (answer.answerHasForQuestion not in questionList):
                            questionList.append(answer.answerHasForQuestion)
                    previousQuestionAndAnswersList = []
                    for question in questionList:
                        nextElementsList.append(question[0])
                        tempList = []
                        for answer in previousAnswerList:
                            if answer.answerHasForQuestion == question:
                                tempList.append(answer)
                        tempList.sort(key=getDisplayIndex)
                        previousQuestionAndAnswersList.append([question[0], tempList])

                    decisionChainList.extend(questionAndAnswer)

            elif len(myList) > 0:
                """
                Si dans decisionChainList, une question est multiple, si la dernière question/réponse le permet, on l'enlève
                """
                s = []
                for question in questionInDecisionChainList:
                    if (getElementForDisplay(question) in list(map(lambda x: getElementForDisplay(x), myList))) and (question not in myList):
                        s.append(question)
                        e = list(filter(lambda x: x.get('question') == question, decisionChainList))
                        decisionChainList.remove(e[0])
                decisionChainList.extend(questionAndAnswer)
                """
                Pour chaque réponse dans decisionChainList, si la réponse n'a pas d'élément suivant,
                ou que les éléments suivant ne sont que des métaux, on l'ajoute à myList
                """
                answersInDecisionChainList = list(map(lambda x: x.get('answer'), decisionChainList))
                myList = []
                for answer in answersInDecisionChainList:
                    if (len(answer.answerHasForNextElement) == 0) or (answer.answerHasForNextElement[0].is_a[0] == onto.Metals):
                        myList.append(answer)
                """
                S'il n'y a qu'un seul élément à myList --> il y a dans decisionChainList un élément final
                et on met les elements suivant dans nextElementList
                Sinon on récupère dans decisionChainList, l'élément le plus avancé
                """
                if len(myList) == 1:
                    nextElementsList = []
                    nextElementsList.extend(myList[0].answerHasForNextElement)
                else:
                    myList2 = []
                    questionsInDecisionChainList = list(map(lambda x: x.get('question'), decisionChainList))

                    for answer in answersInDecisionChainList:
                        nextElementsForAnswer = []
                        for element in answer.answerHasForNextElement:
                            if element.is_a[0] != onto.Metals:
                                nextElementsForAnswer.extend(element.questionHasForNextElements)
                        lastQuestion = True
                        for element in nextElementsForAnswer:
                            if (element.is_a[0] != onto.Metals) and (element in questionsInDecisionChainList):
                                lastQuestion = False
                        if lastQuestion:
                            myList2.append(answer)

                    for answer in myList2:
                        nextElementsList = []
                        for element in answer.answerHasForNextElement:
                            if element.is_a[0] != onto.Metals:
                                nextElementsList.extend(element.questionHasForNextElements)

        """
        On répartit nextElementsList en une liste de question ordonnée et une liste de métaux
        """
        questionsList, metList = sortNextElementsList(nextElementsList)
        questionsList.sort(key=getDisplayIndex)

        questionAndAnswerList = []
        """
        Pour chaque question, 
        si elle fait partie de la liste des question précédente, on ne garde que les réponse possible
        sinon on récupère toutes ses réponses
        """
        for question in questionsList:
            if question in list(map(lambda x: x[0], previousQuestionAndAnswersList)):
                getAnswers = list(filter(lambda x: x[0] == question, previousQuestionAndAnswersList))
                answers = getAnswers[0][1]
            else:
                answers = getAnswersList(question)
            questionAndAnswerList.append(getquestionAndAnswersMap(question, answers))

        """
        On passe questionAndAnswerList dans le context, 
        en faisant en sorte que deux mêmes questions n'apparaissent qu'une seule fois
        """
        for element in questionAndAnswerList:
            listTemp = []
            listTemp.append(getElementForDisplay(element.get('question')))
            listTemp.append(getAnswersForDisplay(element.get('answers')))
            listTemp.append(getQuestionImagePath(element.get('question')))
            if len(listOfQuestionAndAnswerListForContext) == 0:
                listOfQuestionAndAnswerListForContext.append(listTemp)
            else:
                exist = False
                for element2 in listOfQuestionAndAnswerListForContext:
                    if ((listTemp[0] == element2[0]) and (listTemp[1] == element2[1])):
                        exist = True
                if not exist:
                    listOfQuestionAndAnswerListForContext.append(listTemp)

        """
        On passe la chaine de décision dans le contexte
        """
        decisionChainListForContext = getDecisionChainListOfListForContextForLOP(decisionChainList)

        """
        On récupère les listes de métaux recommandé et non recommandé,
        et on les passent dans le contexte
        """
        notSuggestedMetalsList = list(metalsList).copy()
        suggestedMetalsList, notSuggestedMetalsList = getMetalsList(nextElementsList, notSuggestedMetalsList)
        suggestedMetalsListForContext = getMetalsListForContext(suggestedMetalsList)
        notSuggestedMetalsListForContext = getMetalsListForContext(notSuggestedMetalsList)

        s = []

    context = {
        'listOfQuestionAndAnswerListForContext': listOfQuestionAndAnswerListForContext,
        'suggestedMetalsListForContext': suggestedMetalsListForContext,
        'notSuggestedMetalsListForContext': notSuggestedMetalsListForContext,
        'decisionChainListForContext': decisionChainListForContext,
    }

    return render(request, 'ifom_pat/list_of_questions.html', context)




def getQueryGET(request):
    """
    :param request: Objet Request
    :return: String des valeurs de answerQuery, questionQuery et backQuery
    """
    answerQuery = request.GET.get('answer')
    questionQuesry = request.GET.get('question')
    backQuery = request.GET.get('back')
    return answerQuery, questionQuesry, backQuery

def getQuestionByQuestionQuery(questionQuery):
    """
    :param questionQuery: String
    :return: Individu de la classe Questions obtenu à partir de questionQuery
    """
    question = getattr(onto, questionQuery)
    return question

def getAnswerByAnswerQuery(answerQuery):
    """
    :param answerQuery: String
    :return: Individu de la classe Answers obtenu à partir de answerQuery
    """
    answer = getattr(onto, answerQuery)
    return answer



def getDisplayIndex(element):
    """
    :param element: Objet de la classe Answers
    :return: Integer de l'index de trie utile pour l'ordre d'affichage
    """
    return element.hasForDisplayIndex[0]

def getElementForDisplay(element):
    """
    :param element: Individu de la classe Questions, Answers ou Metals
    :return: String à afficher attaché à l'individu.
    """
    return element.hasForDisplay[0]

def getAnswersForDisplay(answersList):
    """
    :param answersList: Liste d'individus de la classe Answers
    :return: Liste de string à afficher pour chaque individu
    """
    answersListForDisplay = list(map(lambda x: getElementForDisplay(x), answersList))
    return answersListForDisplay

def getQuestionImagePath(question):
    """
    :param question: Objet Questions
    :return: String du chemin de l'image attaché à l'individu
    """
    image = question.questionHasForImage[0]
    imagePath = image.hasForPath[0]
    return imagePath



def getNextElements(research):
    """
    On récupère une liste des éléments suivant la réponse donnée
    Cette liste est soit composé d'un seul individu Questions ou d'un ou plusieurs individus Metals
    :param research: Individu de la classe Research
    :return:    nextElements: Une liste composé de 0 ou 1 individu Questions
                nextElementsList: Une liste composé de tous les éléments suivants (Questions ou Metals)
                nextElementsExist: Un booléen précisant s'il y a des éléments suivants
    """
    nextElements = research.researchHasForNextElement
    nextElementsList = []
    nextElementExist = False
    if len(nextElements) > 0:
        if nextElements[0].is_a[0] == onto.Metals:
            nextElementsList = nextElements
            nextElements = []
        else:
            nextElementsList = nextElements[0].questionHasForNextElements
            nextElementExist = True
    return nextElements, nextElementsList, nextElementExist

def getNextElementByQuestion(question):
    """
    :param question: Individu de la classe Questions
    :return: Liste d'individus de la classe Questions et liste d'individus de la classe Metals
    """
    nextQuestionsList = []
    nextMetalsList = []
    for element in question.questionHasForNextElements:
        if element.is_a[0] == onto.Metals:
            nextMetalsList.append(element)
        else:
            nextQuestionsList.append(element)
    return nextQuestionsList, nextMetalsList

def sortNextElementsList(nextElementsList):
    """
    Sépare une liste d'individus de classe Questions et Metals, en une liste de la classe Questions et une de la classe Metals
    :param nextElementsList: Liste d'individus de classe Questions et Metals
    :return: Une liste de la classe Questions et une liste de la classe Metals
    """
    nextQuestionsList = []
    nextMetalsList = []
    for element in nextElementsList:
        if element.is_a[0] == onto.Metals:
            nextMetalsList.append(element)
        else:
            nextQuestionsList.append(element)
    return nextQuestionsList, nextMetalsList



def getAnswersList(question):
    """
    :param question: Individu de la classe Questions
    :return: Liste ordonnée des individs de la classe answers attachés à l'individu Questions
    """
    answers = onto.questionHasForProposedAnswer[question]
    answers.sort(key=getDisplayIndex)
    return answers

def getquestionAndAnswersMap(question, answers):
    """
    :param question: Individu de la classe Questions
    :param answers: Liste d'individus de la classe Answers
    :return: Map au format: { question: <Objet Question>, answers: <Liste d'objet Answers> }
    """
    questionAndAnswersMap = {}
    questionAndAnswersMap['question'] = question
    questionAndAnswersMap['answers'] = answers
    return questionAndAnswersMap

def getQuestionAndAnswersListForContext(question, answers):
    """
    :param question: Individu de la classe Questions
    :param answers: Liste d'individus de la classe Answers
    :return: Liste contenant les éléments suivant:
                [ <Individu de la classe Questions>,
                  <Sting de la question à afficher>,
                  answers = liste contenant: [ <Individus de la classe Answers>, <String de la réponse à afficher> ],
                  <String du path de l'image attaché à l'individu de la classe Questions> ]
    """
    questionAndAnswersListTemp = []
    questionAndAnswersListTemp.append(question.name)
    questionAndAnswersListTemp.append(getElementForDisplay(question))
    answersListTemp = []
    for answer in answers:
        answerListTemp2 = []
        answerListTemp2.append(answer.name)
        answerListTemp2.append(getElementForDisplay(answer))
        answersListTemp.append(answerListTemp2)
    questionAndAnswersListTemp.append(answersListTemp)
    questionAndAnswersListTemp.append(getQuestionImagePath(question))
    return questionAndAnswersListTemp


def getQuestionAndAnswersForContextByQuestionAndAnswersList(questionAndAnswerList):
    """
    :param questionAndAnswerList: Liste de map au format: { question: <Individu de la classe Questions>, answers: <Liste d'individus de la classe Answers> }
    :return: Liste de liste contenant les éléments suivant:
                [ <Individu de la classe Questions>,
                  <Sting de la question à afficher>,
                  answers: Liste de liste contenant: [ <Individus de la classe Answers>, <String de la réponse à afficher> ],
                  <String du path de l'image attaché à l'individu de la classe Questions> ]
    """
    questionAndAnswersListForContext = []
    for element in questionAndAnswerList:
        questionAndAnswersListForContext.append(getQuestionAndAnswersListForContext(element.get('question'), element.get('answers')))
    return questionAndAnswersListForContext

def getQuestionAndAnswersForContextByQuestion(question):
    """
    :param question: Individu de la classe Questions
    :return: Liste de Liste contenant les éléments suivant:
                [ <Individu de la classe Questions>,
                  <Sting de la question à afficher>,
                  answers: Liste de liste contenant: [ <Individus de la classe Answers>, <String de la réponse à afficher> ],
                  <String du path de l'image attaché à l'individu de la classe Questions> ]
    """
    answers = getAnswersList(question)
    questionAndAnswersMap = getquestionAndAnswersMap(question, answers)
    questionAndAnswersList = []
    questionAndAnswersList.append(questionAndAnswersMap)
    questionAndAnswerListForContext = getQuestionAndAnswersForContextByQuestionAndAnswersList(questionAndAnswersList)
    return questionAndAnswerListForContext

def getListOfQuestionAndAnswerListForContext(questionsList):
    """
    A partir d'une liste de question dans laquelle peut figurer des questions multiple,
    retourne une liste contenant des questions à afficher uniques
    :param questionsList: Liste d'individus de la classe Questions
    :return: Liste contenant les éléments suivant:
                [ <String de la question à afficher>,
                  <Liste de String des réponses à afficher>,
                  <String du Path de l'image à afficher>  ]
    """
    listOfQuestionAndAnswerListForContextTemp = []
    for question in questionsList:
        answers = getAnswersList(question)
        listTemp = []
        listTemp.append(getElementForDisplay(question))
        listTemp.append(getAnswersForDisplay(answers))
        listTemp.append(getQuestionImagePath(question))
        if len(listOfQuestionAndAnswerListForContextTemp) == 0:
            listOfQuestionAndAnswerListForContextTemp.append(listTemp)
        else:
            exist = False
            for element in listOfQuestionAndAnswerListForContextTemp:
                if ((listTemp[0] == element[0]) and (listTemp[1] == element[1])):
                    exist = True
            if not exist:
                listOfQuestionAndAnswerListForContextTemp.append(listTemp)
    return listOfQuestionAndAnswerListForContextTemp



def getDecisionChainMap(question, answer):
    """
    :param question: Individu de la classe Questions
    :param answer: Individu de la classe Answers
    :return: Map au format: { question: <Individu de la classe Questions>, answer: <Individu de la classe Answers> }
    """
    decisionChainMap = {}
    decisionChainMap['question'] = question
    decisionChainMap['answer'] = answer
    return decisionChainMap

def getDecisionChaiListForContext(decisionChaiMap):
    """
    :param decisionChaiMap: Map au format: { question: <Individu de la classe Questions>, answer: <Individu de la classe Answers> }
    :return: Liste contenant les éléments suivant:
                [ <Individu de la classe Questions>,
                  <String de la question à afficher>,
                  <Individu de la classe Answers>
                  <String de la réponse à afficher> ]
    """
    decisionChainListForContext = []
    decisionChainListForContext.append(decisionChaiMap['question'].name)
    decisionChainListForContext.append(getElementForDisplay(decisionChaiMap['question']))
    decisionChainListForContext.append(decisionChaiMap['answer'].name)
    decisionChainListForContext.append(getElementForDisplay(decisionChaiMap['answer']))
    return decisionChainListForContext

def getDecisionChaiListForContextForLOP(decisionChainMap):
    """
    :param decisionChaiMap: Map au format: { question: <Individu de la classe Questions>, answer: <Individu de la classe Answers> }
    :return: Liste contenant les éléments suivant:
                  <String de la question à afficher>,
                  <String de la réponse à afficher> ]
    """
    decisionChainListForContext = []
    decisionChainListForContext.append(getElementForDisplay(decisionChainMap['question']))
    decisionChainListForContext.append(getElementForDisplay(decisionChainMap['answer']))
    return decisionChainListForContext



def getDecisionChainListOfListForContext(decisionChainList):
    """
    :param decisionChainList: Liste de map au format: { question: <Individu de la classe Questions>, answer: <Individu de la classe Answers> }
    :return: Liste de liste au format:
                [ <Individu de la classe Questions>,
                  <String de la question à afficher>,
                  <Individu de la classe Answers>
                  <String de la réponse à afficher> ]
    """
    decisionChainListForContext = []
    for element in decisionChainList:
        myListe = getDecisionChaiListForContext(element)
        if myListe not in decisionChainListForContext:
            decisionChainListForContext.append(getDecisionChaiListForContext(element))
    return decisionChainListForContext

def getDecisionChainListOfListForContextForLOP(decisionChainList):
    """
    :param decisionChainList: Liste de map au format: { question: <Individu de la classe Questions>, answer: <Individu de la classe Answers> }
    :return: Liste de liste au format:
                  <String de la question à afficher>,
                  <String de la réponse à afficher> ]
    """
    decisionChainListForContext = []
    for element in decisionChainList:
        myListe = getDecisionChaiListForContextForLOP(element)
        if myListe not in decisionChainListForContext:
            decisionChainListForContext.append(getDecisionChaiListForContextForLOP(element))
    return decisionChainListForContext

def getNewDecisionChainList(decisionChainList, questionQuery):
    """
    :param decisionChainList: Liste de map au format: { question: <Individu de la classe Questions>, answer: <Individu de la classe Answers> }
    :param questionQuery: String
    :return: Liste de map au format: { question: <Individu de la classe Questions>, answer: <Individu de la classe Answers> }
    """
    decisionChainTemp = []
    for element in decisionChainList:
        if element['question'].name == questionQuery:
            break
        else:
            decisionChainTemp.append(element)
    decisionChainList = decisionChainTemp
    return decisionChainList



def getSearchHistoryList(decisionChainList, suggestedMetalsList, notSuggestedMetalsList):
    """
    :param decisionChainList: Liste de map au format: { question: <Individu de la classe Questions>, answer: <Individu de la classe Answers> }
    :param suggestedMetalsList: Liste d'Individu de la classe Metals
    :param notSuggestedMetalsList: Liste d'Individu de la classe Metals
    :return: Liste de liste au format:
                [ <Individu de la classe Questions>,
                  <String de la question à afficher>,
                  <Individu de la classe Answers>
                  <String de la réponse à afficher> ],
                [ <String à afficher>, <String du path de la fiche PDF> ],
                [ <String à afficher>, <String du path de la fiche PDF> ]
    """
    global searchHistoryListForContext
    decisionChainAndMetalsHistory = []
    decisionChainHistory = getDecisionChainListOfListForContext(decisionChainList)
    suggestedMetalsListHistory = getMetalsListForContext(suggestedMetalsList)
    notSuggestedMetalsListHistory = getMetalsListForContext(notSuggestedMetalsList)
    decisionChainAndMetalsHistory.append(decisionChainHistory)
    decisionChainAndMetalsHistory.append(suggestedMetalsListHistory)
    decisionChainAndMetalsHistory.append(notSuggestedMetalsListHistory)
    searchHistoryListForContext.append(decisionChainAndMetalsHistory)
    return searchHistoryListForContext



def getMetalsList(nextElementsList, notSuggestedMetalsList):
    """
    :param nextElementsList: Liste d'individu de la classe Questions ou d'individu de la classe Metals
    :param notSuggestedMetalsList: Liste d'Individu de la classe Metals
    :return: Liste d'Individu de la classe Metals suggéré et une liste d'Individu de la classe Metals non suggéré
    """
    suggestedMetalsList = []
    for element in nextElementsList:
        if element.is_a[0] == onto.Metals:
            suggestedMetalsList.append(element)
            notSuggestedMetalsList.remove(element)
    return suggestedMetalsList, notSuggestedMetalsList

def getMetalDocSheetPath(metal):
    """
    :param metal: Individu de la classe Metals
    :return: String du path de la fiche PDF du métal
    """
    metalDocSheet = metal.metalHasForDocumentarySheet[0]
    metalDocSheetPath = metalDocSheet.hasForPath[0]
    return metalDocSheetPath

def getMetalsListForContext(metalsList):
    """
    :param metalsList: Liste d'Individu de la classe Metals
    :return: Liste de liste contenant les éléments suivant: [ <String à afficher>, <String du path de la fiche PDF> ]
    """
    metalsListForContextTemp = []
    for metal in metalsList:
        metalListTemp = []
        metalListTemp.append(metal.hasForDisplay[0])
        metalListTemp.append(getMetalDocSheetPath(metal))
        metalsListForContextTemp.append(metalListTemp)
    return metalsListForContextTemp



