from django.apps import AppConfig


class IfomPatConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ifom_pat'
