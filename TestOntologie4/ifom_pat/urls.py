
from django.conf.urls import url

from ifom_pat import views

urlpatterns = [

    url(r'^$', views.index, name='index'),
    url(r'^by_sequential_question/$', views.sequentialQuestion, name='sequentialQuestion'),
    url(r'^by_list_of_questions/$', views.listOfQuestions, name='listOfQuestions'),

]


